#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int main() {
  srand(time(0));
  // generate expressions with i args
  for (int i = 2; i < 10; i++) {
    int arr[i];
    size_t tries = 0;
    // generate 1000 expressions
    for (size_t j = 0; j < 1000; tries++) {
      arr[0] = rand() % 3 + 1;
      for (int k = 1; k < i; k++) {
        switch (rand() % 6) {
          case 0: arr[k] = -3; break;
          case 1: arr[k] = -2; break;
          case 2: arr[k] = -1; break;
          case 3: arr[k] = 1; break;
          case 4: arr[k] = 2; break;
          case 5: arr[k] = 3; break;
          /*default: __builtin_unreachable();*/
        }
      }
      int sum = 0;
      for (int k = 0; k < i; k++)
        sum += arr[k];
      if (sum > 0 && sum < 4) {
        /*
        printf("%d", arr[0]);
        for (int k = 1; k < i; k++) printf("%+d", arr[k]);
        printf("==%d\n", sum);
        */
        j++;
      }
    }
    printf("%d args %zu tries\n", i, tries);
  }
}
